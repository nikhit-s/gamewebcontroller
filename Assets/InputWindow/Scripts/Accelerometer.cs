﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Myapp {
    
public class Accelerometer : MonoBehaviour
{
    // Move object using accelerometer
    float speed = 10.0f;
    public Text ftext;

    void Update()
    {
        Vector3 dir = Vector3.zero;

        dir.x = -Input.acceleration.y ;
        dir.z = Input.acceleration.x ;
        dir.y = Input.acceleration.z ;

        // clamp acceleration vector to unit sphere
        if (dir.sqrMagnitude > 1)
            dir.Normalize();

        // Make it move 10 meters per second instead of 10 meters per frame...
        //dir *= Time.deltaTime;

        // Move object
        transform.Translate(dir * speed);
        ftext.text= dir.x + "," + dir.z + ","+ dir.y;
        //Debug.Log(ftext);
        WsClient.WS.Send(ftext.text);
    }
}
}