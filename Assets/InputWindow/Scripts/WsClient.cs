using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using WebSocketSharp;
//using System.Net.WebSockets;
using UnityEngine.UI;
using System.Threading;


namespace Myapp {
    public class WsClient : MonoBehaviour
{
    public InputField IP;
    public InputField port;
    public Text fText;
    public static WebSocket ws;

    public static WebSocket WS{
        get{
            return ws;
        }
    }
    //public Button_UI okBtn;
    private void StartWS()
    {
        ws = new WebSocket(String.Format("ws://{0}:{1}", IP.text, port.text));
        ws.Connect();
        if (ws.IsAlive == false)
            {
                throw new Exception("Connection not established");
            }
        
    }
    private void Update()
    {
        
        if(ws == null)
        {
            return;
        }
        else{
            //Thread.Sleep(10000);
            //ws.Send("Hello");
            fText.text="Connected..";
            ws.OnMessage += (sender, e) =>
        {
            Debug.Log("Message Received from "+((WebSocket)sender).Url+", Data : "+e.Data);
            
        };

        }
    }

    public void OnButtonPress()
    {
        try{
            StartWS();
        }
        catch(Exception e){
            Debug.Log(e.Message);
            fText.text="Failed..";
            ws.Close();
        }
        

    }

            
        
    
}


}
