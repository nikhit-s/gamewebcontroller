# SET UP

## Pre requisite :

Need to install NodeJs : https://nodejs.org/en/

Both PC and mobile should be under same network.

**Step 1**:

Open terminal and navigate to \GameWebControler\NodeModule> using cd

Type "node websocket.js" and press enter

Open index.html from GameWebController folder

**Step 2**:

Open mobile app

Connect with computer by typing the ip address and port number which will be shown in the terminal (Default Port Number :
43222).

Press Connect and wait for "Connected.." Message 

And

Press Home button once.

Once home page loaded select game and Enjoy your game.
