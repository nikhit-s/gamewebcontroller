const WebSocket = require('ws')
const os = require('os');

const wss = new WebSocket.Server({ port: 43222 },()=>{
    console.log('server started')
})
wss.on('connection', (ws) => {
   ws.on('message', (data) => {
      console.log('data received \n '+ data)
      wss.clients.forEach(function(client) {
         client.send(data.toString());
      });
   })
})
wss.on('listening',()=>{
   console.log('IP Address is : ' + JSON.stringify(os.networkInterfaces()['Wi-Fi']).match(/"192.168.\d+.\d+"/g)[0] + '  Please use this to connect with App')

   console.log('listening on 43222')
})
